import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { useForm } from 'react-hook-form';


const ItemComponent = () => {
  const [items, setItems] = useState([]);
  const [selectedItemId, setSelectedItemId] = useState(null);
  const [selectedCat2Id, setSelectedCat2d] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);
  const { register, handleSubmit,reset, formState: { errors } } = useForm();
  const [categorySubLevels, setCategorySubLevels] = useState([]);
  const [categorySubLevels2, setCategorySubLevels2] = useState([]);

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editItemData, setEditItemData] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        await Promise.all([
          fetchItems(),
          fetchCategorySubLevels(),
          fetchCategorySubLevels2()
        ]);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
  
    fetchData();
  }, []);

  
  

  const fetchItems = async () => {
    try {
      const response = await fetch('http://localhost:3000/items');
      const data = await response.json();
      if (!response.ok) {
        throw new Error('Failed to fetch Item sub levels');
      }
      setItems(data);
    } catch (error) {
      console.error('Error fetching items:', error);
    }
  };

  const fetchCategorySubLevels = async () => {
    try {
      const response = await fetch('http://localhost:3000/category1');
      if (!response.ok) {
        throw new Error('Failed to fetch category sub levels');
      }
      const data = await response.json();
      setCategorySubLevels(data);
    } catch (error) {
      // console.error('Error fetching category sub levels:', error);
    }
  };

  const fetchCategorySubLevels2 = async () => {
    try {
      const response = await fetch('http://localhost:3000/category2');
      const data = await response.json();
      if (!response.ok) {
        throw new Error('Failed to fetch category sub levels');
      }
      setCategorySubLevels2(data);
    } catch (error) {
      // console.error('Error fetching category sub levels:', error);
    }
  };

  const handleDelete = async () => {
    try {
      await fetch(`http://localhost:3000/items/${selectedItemId}`, {
        method: 'DELETE',
      });
      setIsDeleteModalOpen(false);
      fetchItems();
    } catch (error) {
      console.error('Error deleting item:', error);
    }
  };

  const handleOpenDeleteModal = (id) => {
    setSelectedItemId(id);
    setIsDeleteModalOpen(true);
  };

  const handleCloseDeleteModal = () => {
    setSelectedItemId(null);
    setIsDeleteModalOpen(false);
  };

  const handleOpenCreateModal = () => {
    setIsCreateModalOpen(true);
  };

  const handleCloseCreateModal = () => {
    setIsCreateModalOpen(false);
  };

  const handleCloseModalAndResetForm = () => {
    setIsCreateModalOpen(false) && reset();
  }
  const handleCloseEditModalAndResetForm = () => {
    handleCloseEditModal() && reset();
  }

  const columns = [
    { field: 'id', headerName: 'ID', width: 70 ,align: 'center' },
    {
      field: 'base64Image',
      headerName: 'Item Image',
      align: 'center',
      renderCell: (params) => (
        params.value ? 
          <img src={`${params.value}`} alt="Item Image" style={{ width: '50px', height: '50px' }} /> 
          : 
          null
      ),
    },
    { field: 'name', headerName: 'Name', width: 130 },
    { field: 'amount', headerName: 'Amount', type: 'number', width: 90 },
    { field: 'price', headerName: 'Price', type: 'number', width: 90 },
    { field: 'costPrice', headerName: 'Cost Price', type: 'number', width: 110 },
    { 
      field: 'categorySubLevel1Names', 
      headerName: 'Category Sub Level 1', 
      width: 150,
      align: 'center', 
    },
    { 
      field: 'categorySubLevel2Names', 
      headerName: 'Category Sub Level 2', 
      width: 150,
      align: 'center',
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 120,
      align: 'center',
      renderCell: (params) => (
        <div className='d-flex justify-content-center'>
         <button className='btn btn-danger me-1' onClick={() => handleOpenDeleteModal(params.row.id)}>DEL</button>
          <button className='btn btn-primary' onClick={() => handleOpenEditModal(params.row)}>Edit</button>
       </div>
      ),
    },
  ];

  const rows = items.map(item => ({
    id: item.id,
    name: item.name,
    amount: item.amount,
    price: item.price,
    costPrice: item.cost_price,
    categorySubLevel1Names: item.category_sub_level_1_names ,
    categorySubLevel2Names: item.category_sub_level_2_names ,
    categorySubLevel2: item.category_sub_level_2 ,
    categorySubLevel1: item.category_sub_level_1 ,
    base64Image: item.base64Image ? `data:image/png;base64,${item.base64Image}` : null,
  }));

  const [selectedCategorySubLevels, setSelectedCategorySubLevels] = useState([]);

  const handleChange = (event) => {
    setSelectedCategorySubLevels(event.target.value);
  };


  useEffect(() => {
    let str_array = stringToArray(editItemData?.categorySubLevel1);
  
    let cat2item = [];
    str_array.forEach(i =>
      categorySubLevels.find(j => {
        if (j.id === i) {
          let _array = stringToArray(j.sub_level_2_id_list)
          _array.map(i=>cat2item.push(i))
        }
      })
    );
   
  }, [editItemData]);

  const handleEditChange = (event) => {
    let cat2item = [];
    event.target.value.forEach(i =>
      categorySubLevels.find(j => {
        if (j.id === i) {
          let _array = stringToArray(j.sub_level_2_id_list)
          _array.map(i=>cat2item.push(i))
        }
      })
    );

    setEditItemData(editItemData => ({
      ...editItemData,
      categorySubLevel1 : arrayToString(event.target.value) ,
      categorySubLevel2 : arrayToString(cat2item)
  }))

    
  };

  useEffect(() => {
    let cat2item = [];
    selectedCategorySubLevels.forEach(i =>
      categorySubLevels.find(j => {
        if (j.id === i) {
          let _array = stringToArray(j.sub_level_2_id_list)
          _array.map(i=>cat2item.push(i))
        }
      })
    );
    setSelectedCat2d(cat2item)
  }, [selectedCategorySubLevels, categorySubLevels2]);



  const onSubmit = async (data) => {
    try {
      let formData = new FormData();

      if (data.item_img && data.item_img.length > 0) {
        formData.append('item_img', data.item_img[0]);
      } else {
        formData.append('item_img', ''); 
      }

      formData.append('name', data.name);
      formData.append('amount', data.amount);
      formData.append('price', data.price);
      formData.append('cost_price', data.cost_price);
      formData.append('category_sub_level_1', JSON.stringify(data.category_sub_level_1));

      const response = await fetch('http://localhost:3000/items', {
        method: 'POST',
        body: formData 
      });
      if (response.ok) {
        fetchItems();
        alert('Item created successfully');
        handleCloseModalAndResetForm();
      }else {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

    } catch (error) {
      console.error('Error creating item:', error);
      alert('Failed to create item: ' + error.message);
    }
  };

  const onEditSubmit = async (data) => {
    try {
      let formData = new FormData();

      if (data.item_img && data.item_img.length > 0) {
        formData.append('item_img', data.item_img[0]);
      } else {
        formData.append('item_img', ''); 
      }

      formData.append('name', data.name);
      formData.append('amount', data.amount);
      formData.append('price', data.price);
      formData.append('cost_price', data.cost_price);
      formData.append('category_sub_level_1', JSON.stringify(data.category_sub_level_1));

      const response = await fetch(`http://localhost:3000/items/${editItemData.id}`, {
        method: 'PUT',
        body: formData 
      });
      if (response.ok) {
        fetchItems();
        alert('Item update successfully');
        handleCloseEditModalAndResetForm();
      }else {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

    } catch (error) {
      console.error('Error creating item:', error);
      alert('Failed to update item: ' + error.message);
    }
  };

  const stringToArray = (str) => {
    if (!str) return [];
    return str.split(',').map(Number);
    };
    const arrayToString = (array) => {
      return array.join(',');
    };


    const handleOpenEditModal = (item) => {
      setEditItemData(item);
      setIsEditModalOpen(true);
    };

    const handleCloseEditModal = () => {
      setEditItemData(null);
      setIsEditModalOpen(false);
      reset(); 
    };


  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: { paginationModel: { pageSize: 10 } },
        }}
        pageSizeOptions={[10, 25]}
      />


      <div className='bg-white p-2 my-2 rounded'>
        <Button className='w-100' variant="contained" onClick={handleOpenCreateModal}>Create</Button>
      </div>



      {/* model create */}
      <Modal
        open={isCreateModalOpen}
        onClose={handleCloseCreateModal}
        aria-labelledby="create-item-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="create-item-modal">Create Item</h2>
          <form  onSubmit={handleSubmit(onSubmit)}>

            <div className='mb-3 text-start'>
              <label className="form-label w-100">Name</label>
              <input className='form-control' type="text" {...register('name', { 
                required: 'Name is required',
                minLength: { value: 3, message: 'Name must be at least 3 characters' },
                maxLength: { value: 100, message: 'Name must not exceed 100 characters' },
                pattern: { 
                  value: /^[A-Za-z0-9ก-๙\s]+$/,
                  message: 'Name must contain only letters, numbers, and Thai characters'
                }
              })} />
              {errors.name && <span className='text-danger w-100'>{errors.name.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Img</label>
              <input className='form-control' type="file" accept=".png, .jpg" {...register('item_img', {
                validate: {
                  lessThan10MB: value => !value || (value.length === 0 || (value[0] && value[0].size <= 10000000)) || 'Image size must be less than 10MB',
                  acceptedFormats: value => !value || (value.length === 0 || (value[0] && ['image/png', 'image/jpeg'].includes(value[0].type))) || 'Only PNG and JPG formats are allowed'
                }
              })} />
              {errors.item_img && <span className='text-danger w-100'>{errors.item_img.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Amount</label>
              <input className='form-control' type="number" {...register('amount', {
                required: 'Amount is required',
                pattern: {
                  value: /^[0-9]+$/,
                  message: 'Amount must contain only numbers'
                }
              })} />
              {errors.amount && <span className='text-danger w-100'>{errors.amount.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Price</label>
              <input className='form-control' type="number" {...register('price', {
                required: 'Price is required',
                pattern: {
                  value: /^\d*\.?\d*$/,
                  message: 'Price must be a valid number'
                }
              })} />
              {errors.price && <span className='text-danger w-100'>{errors.price.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cost Price</label>
              <input className='form-control' type="number" {...register('cost_price', {
                required: 'Cost Price is required',
                pattern: {
                  value: /^\d*\.?\d*$/,
                  message: 'Cost Price must be a valid number'
                }
              })} />
              {errors.cost_price && <span className='text-danger w-100'>{errors.cost_price.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cat1</label>
              <Select
                className='form-select'
                multiple
                value={selectedCategorySubLevels}
                onChange={handleChange}
                inputProps={{ ...register('category_sub_level_1') }} 
              >
                 {Array.isArray(categorySubLevels) && categorySubLevels.map((category) => (
                <MenuItem key={category.id} value={category.id}>
                  {category.name}
                </MenuItem>
              ))}
              </Select>

            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cat2(auto fill)</label>
                <Select
                    className='form-select w-100'
                    multiple
                    value={selectedCat2Id}
                    onChange={handleChange}
                    inputProps={{ ...register('category_sub_level_2_id') }} 
                    disabled
                    >
                    {Array.isArray(categorySubLevels2) && categorySubLevels2.map((category) => (
                        <MenuItem key={category.id} value={category.id}>
                        {category.name}
                        </MenuItem>
                    ))}
                </Select>
            </div>

            <Button className='me-1' type="submit" variant="contained">Submit</Button>
            <Button variant="contained" onClick={handleCloseCreateModal}>Cancel</Button>
          </form>
        </div>
      </Modal>

      {/* model delete */}
      <Modal
        open={isDeleteModalOpen}
        onClose={handleCloseDeleteModal}
        aria-labelledby="confirm-delete-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="confirm-delete-modal">Confirm Delete</h2>
          <p>Are you sure you want to delete this item?</p>
          <Button variant="contained" onClick={handleDelete}>Delete</Button>
          <Button variant="contained" onClick={handleCloseDeleteModal}>Cancel</Button>
        </div>
      </Modal>

      {/* model edit */}
      <Modal
        open={isEditModalOpen}
        onClose={handleCloseEditModal}
        aria-labelledby="edit-item-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >

        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="create-item-modal">Edit Item</h2>
            <form  onSubmit={handleSubmit(onEditSubmit)}>
              
              
            <div className='mb-3 text-start'>
              <label className="form-label w-100">Name</label>
              <input className='form-control' 
              defaultValue={editItemData?.name}
              type="text" {...register('name', { 
                required: 'Name is required',
                minLength: { value: 3, message: 'Name must be at least 3 characters' },
                maxLength: { value: 100, message: 'Name must not exceed 100 characters' },
                pattern: { 
                  value: /^[A-Za-z0-9ก-๙\s]+$/,
                  message: 'Name must contain only letters, numbers, and Thai characters'
                }
              })} />
              {errors.name && <span className='text-danger w-100'>{errors.name.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Img</label>
              <input className='form-control'  type="file" accept=".png, .jpg" {...register('item_img', {
                validate: {
                  lessThan10MB: value => !value || (value.length === 0 || (value[0] && value[0].size <= 10000000)) || 'Image size must be less than 10MB',
                  acceptedFormats: value => !value || (value.length === 0 || (value[0] && ['image/png', 'image/jpeg'].includes(value[0].type))) || 'Only PNG and JPG formats are allowed'
                }
              })} />
              {errors.item_img && <span className='text-danger w-100'>{errors.item_img.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Amount</label>
              <input className='form-control' type="number" defaultValue={editItemData?.amount} {...register('amount', {
                required: 'Amount is required',
                pattern: {
                  value: /^[0-9]+$/,
                  message: 'Amount must contain only numbers'
                }
              })} />
              {errors.amount && <span className='text-danger w-100'>{errors.amount.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Price</label>
              <input className='form-control' type="number" defaultValue={editItemData?.price} {...register('price', {
                required: 'Price is required',
                pattern: {
                  value: /^\d*\.?\d*$/,
                  message: 'Price must be a valid number'
                }
              })} />
              {errors.price && <span className='text-danger w-100'>{errors.price.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cost Price</label>
              <input className='form-control' type="number" defaultValue={editItemData?.costPrice} {...register('cost_price', {
                required: 'Cost Price is required',
                pattern: {
                  value: /^\d*\.?\d*$/,
                  message: 'Cost Price must be a valid number'
                }
              })} />
              {errors.cost_price && <span className='text-danger w-100'>{errors.cost_price.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cat1</label>
              <Select
                className='form-select'
                multiple
                value={stringToArray(editItemData?.categorySubLevel1)}
                onChange={handleEditChange}
                inputProps={{ ...register('category_sub_level_1') }} 
              >
                 {Array.isArray(categorySubLevels) && categorySubLevels.map((category) => (
                <MenuItem key={category.id} value={category.id}>
                  {category.name}
                </MenuItem>
              ))}
              </Select>

            </div>

            <div className='mb-3 text-start'>
              <label className="form-label text-start w-100">Cat2(auto fill)</label>
                <Select
                    className='form-select w-100'
                    multiple
                    value={stringToArray(editItemData?.categorySubLevel2)}
                    inputProps={{ ...register('category_sub_level_2_id') }} 
                    disabled
                    >
                    {Array.isArray(categorySubLevels2) && categorySubLevels2.map((category) => (
                        <MenuItem key={category.id} value={category.id}>
                        {category.name}
                        </MenuItem>
                    ))}
                </Select>
            </div>


              <Button className='me-1' type="submit" variant="contained">Submit</Button>
              <Button variant="contained" onClick={handleCloseEditModal}>Cancel</Button>
            </form>
          </div>
        
      </Modal>
    </div>
  );
};

export default ItemComponent;
