import React , { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { Button } from '@mui/material';
import Modal from '@mui/material/Modal';
import { useForm } from 'react-hook-form';

const Cat2Component = () => {

  const [data, setData] = useState([]);
  const [selectedItemId, setSelectedItemId] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  const [isCreateModalOpen, setIsCreateModalOpen] = useState(false);

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [editItemData, setEditItemData] = useState(null);

  const { register, handleSubmit,reset, formState: { errors } } = useForm();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('http://localhost:3000/category2');
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const jsonData = await response.json();
      setData(jsonData);
    } catch (error) {
      console.error('Error fetching data:', error);
      throw error;
    }
  };
  

  const columns = [
    { field: 'id', headerName: 'ID', width: 100 , align: 'center',  headerAlign: 'center' },
    { field: 'name', headerName: 'Name', width: 200  ,align: 'center',  headerAlign: 'center' },
    { field: 'desc', headerName: 'Description', width: 300 ,  align: 'center', headerAlign: 'center'},
    {
      field: 'actions',
      headerName: 'Actions',
      headerAlign: 'center',
      width: 200,
      renderCell: (params) => (
       <div className='d-flex justify-content-center'>
         <button className='btn btn-danger me-1' onClick={() => handleOpenDeleteModal(params.row.id)}>DEL</button>
          <button className='btn btn-primary' onClick={() => handleOpenEditModal(params.row)}>Edit</button>
       </div>
      )
    }
  ];

  const rows = data.map(item => ({
    id: item.id,
    name: item.name,
    desc: item.desc
  }));


  const handleDelete = async () => {
    try {
      const response = await fetch(`http://localhost:3000/category2/${selectedItemId}`, {
        method: 'DELETE',
      });
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      fetchData();
      setIsDeleteModalOpen(false);
    } catch (error) {
      console.error('Error deleting data:', error);
    }
  };


  const onSubmit = async (data) => {
    try {

      let payload = {
        name : data.name ,
        desc : data.desc
      }
      
      const response = await fetch('http://localhost:3000/category2', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(payload)
      });
      console.log(response.ok)
      if (response.ok) {
        fetchData();
        alert('Item created successfully');
        handleCloseModalAndResetForm();
      }else {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

    } catch (error) {
      console.error('Error creating item:', error);
      alert('Failed to create item: ' + error.message);
    }
  };

  const handleOpenDeleteModal = (id) => {
    setSelectedItemId(id);
    setIsDeleteModalOpen(true);
  };

  const handleCloseDeleteModal = () => {
    setSelectedItemId(null);
    setIsDeleteModalOpen(false);
  };

  const handleOpenCreateModal = () => {
    setIsCreateModalOpen(true);
  };

  const handleCloseCreateModal = () => {
    setIsCreateModalOpen(false);
  };

  const handleCloseModalAndResetForm = () => {
    setIsCreateModalOpen(false) && reset();
  }

  const handleOpenEditModal = (item) => {
    setEditItemData(item);
    setIsEditModalOpen(true);
  };

  const handleCloseEditModal = () => {
    setEditItemData(null);
    setIsEditModalOpen(false);
    reset(); 
  };


  const onEditSubmit = async (data) => {
    try {
      const response = await fetch(`http://localhost:3000/category2/${editItemData.id}`, {
        method: 'PUT', // Assuming you use PUT method for editing
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
      if (response.ok) {
        fetchData();
        alert('Item updated successfully');
        handleCloseEditModal();
      } else {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }
    } catch (error) {
      console.error('Error updating item:', error);
      alert('Failed to update item: ' + error.message);
    }
  };


  return (
    <div style={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: { paginationModel: { pageSize: 5 } },
        }}
        pageSizeOptions={[5, 10, 25]}
      />

      <div className='bg-white p-2 my-2 rounded'>
        <Button className='w-100' variant="contained" onClick={handleOpenCreateModal}>Create</Button>
      </div>

      {/* create modal */}
      <Modal
        open={isCreateModalOpen}
        onClose={handleCloseCreateModal}
        aria-labelledby="create-item-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="create-item-modal">Create Item</h2>
          <form  onSubmit={handleSubmit(onSubmit)}>

            <div className='mb-3 text-start'>
                <label className="form-label w-100">Name</label>
                <input className='form-control' type="text" {...register('name', { 
                  required: 'Name is required',
                  minLength: { value: 3, message: 'Name must be at least 3 characters' },
                  maxLength: { value: 100, message: 'Name must not exceed 100 characters' },
                  pattern: { 
                    value: /^[A-Za-z0-9ก-๙\s]+$/,
                    message: 'Name must contain only letters, numbers, and Thai characters'
                  }
                })} />
                {errors.name && <span className='text-danger w-100'>{errors.name.message}</span>}
              </div>

              <div className='mb-3 text-start'>
              <label className="form-label w-100">Description</label>
              <input className='form-control' type="text" {...register('desc', { 
                pattern: { 
                  value: /^[A-Za-z0-9ก-๙\s]+$/,
                  message: 'desc must contain only letters, numbers, and Thai characters'
                }
              })} />
              {errors.desc && <span className='text-danger w-100'>{errors.desc.message}</span>}
            </div>

            <Button className='me-1' type="submit" variant="contained">Submit</Button>
            <Button variant="contained" onClick={handleCloseCreateModal}>Cancel</Button>
          </form>
        </div>
      </Modal>

      {/* delete modal */}
      <Modal
        open={isDeleteModalOpen}
        onClose={handleCloseDeleteModal}
        aria-labelledby="confirm-delete-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="confirm-delete-modal">Confirm Delete</h2>
          <p>Are you sure you want to delete this item?</p>
          <Button className='me-1' variant="contained" onClick={handleDelete}>Delete</Button>
          <Button variant="contained" onClick={handleCloseDeleteModal}>Cancel</Button>
        </div>
      </Modal>


      {/* Edit Modal */}
      <Modal
        open={isEditModalOpen}
        onClose={handleCloseEditModal}
        aria-labelledby="edit-item-modal"
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
      >
        <div style={{ backgroundColor: 'white', padding: '20px', borderRadius: '8px', textAlign: 'center' }}>
          <h2 id="edit-item-modal">Edit Item</h2>
          <form onSubmit={handleSubmit(onEditSubmit)}>
            <div className='mb-3 text-start'>
              <label className="form-label w-100">Name</label>
              <input className='form-control' type="text" defaultValue={editItemData?.name} {...register('name', { 
                required: 'Name is required',
                minLength: { value: 3, message: 'Name must be at least 3 characters' },
                maxLength: { value: 100, message: 'Name must not exceed 100 characters' },
                pattern: { 
                  value: /^[A-Za-z0-9ก-๙\s]+$/,
                  message: 'Name must contain only letters, numbers, and Thai characters'
                }
              })} />
              {errors.name && <span className='text-danger w-100'>{errors.name.message}</span>}
            </div>

            <div className='mb-3 text-start'>
              <label className="form-label w-100">Description</label>
              <input className='form-control' type="text" defaultValue={editItemData?.desc} {...register('desc', {
                pattern: {
                  value: /^[A-Za-z0-9ก-๙\s]+$/,
                  message: 'Description must contain only letters, numbers, and Thai characters'
                }
              })} />
              {errors.desc && <span className='text-danger w-100'>{errors.desc.message}</span>}
            </div>

            <Button className='me-1' type="submit" variant="contained">Submit</Button>
            <Button variant="contained" onClick={handleCloseEditModal}>Cancel</Button>
          </form>
        </div>
      </Modal>


    </div>
  );
};

export default Cat2Component;
