import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@mui/material/styles';

import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

import Cat1Component from './component/cat1';
import Cat2Component from './component/cat2';
import ItemComponent from './component/item';


function App() {
  return (
    <Router>
      <div className="d-flex flex-column align-items-center bg-danger" style={{ height: "100vh" }}>
        <nav className="navbar navbar-expand-lg navbar-light bg-light w-100">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">Navbar</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <Link className="nav-link" to="/">Items</Link>
              <Link className="nav-link" to="/cat1">cat1</Link>
              <Link className="nav-link" to="/cat2">cat2</Link>
            </div>
          </div>
        </div>
      </nav>
      <div className='bg-primary w-100 h-100 d-flex align-items-center justify-content-center'>
          <div className="card">
            <Routes>
              <Route path="/" element={<ItemComponent />} />
              <Route path="/cat1" element={<Cat1Component />} />
              <Route path="/cat2" element={<Cat2Component />} />
            </Routes>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
