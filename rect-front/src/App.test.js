import { render, screen ,waitFor,fireEvent} from '@testing-library/react';
import App from './App';
import '@testing-library/jest-dom/extend-expect';
import Cat2Component from './component/cat2';

test('renders navigation links', () => {
  render(<App />);
  
  // Find the navigation links
  const itemsLink = screen.getByText(/Items/i);
  const cat1Link = screen.getByText(/cat1/i);
  const cat2Link = screen.getByText(/cat2/i);

  // Assert that the links are in the document
  expect(itemsLink).toBeInTheDocument();
  expect(cat1Link).toBeInTheDocument();
  expect(cat2Link).toBeInTheDocument();
});

